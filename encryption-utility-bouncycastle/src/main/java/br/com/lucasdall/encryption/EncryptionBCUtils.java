package br.com.lucasdall.encryption;

import java.io.IOException;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.Collection;

import org.bouncycastle.cms.CMSAlgorithm;
import org.bouncycastle.cms.CMSEnvelopedData;
import org.bouncycastle.cms.CMSEnvelopedDataGenerator;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSTypedData;
import org.bouncycastle.cms.KeyTransRecipientInformation;
import org.bouncycastle.cms.RecipientInformation;
import org.bouncycastle.cms.jcajce.JceCMSContentEncryptorBuilder;
import org.bouncycastle.cms.jcajce.JceKeyTransEnvelopedRecipient;
import org.bouncycastle.cms.jcajce.JceKeyTransRecipient;
import org.bouncycastle.cms.jcajce.JceKeyTransRecipientInfoGenerator;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.OutputEncryptor;

public class EncryptionBCUtils {

	/**
	 * Configure the BouncyCastleProvider
	 */
	static {
		Security.addProvider(new BouncyCastleProvider());
	}
	
	/**
	 * 
	 * 
	 * @param keystoreFile
	 * @param password
	 * @param keyStoreType
	 * @return
	 * @throws KeyStoreException
	 * @throws IOException
	 * @throws NoSuchAlgorithmException
	 * @throws CertificateException
	 */
	public static KeyStore loadKeyStore(final InputStream keystoreFile, final String password, final String keyStoreType) throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException {
		final KeyStore keystore = KeyStore.getInstance(keyStoreType);
		keystore.load(keystoreFile, null == password ? null : password.toCharArray());
		return keystore;
	}

	public static KeyPair getKeyPair(final KeyStore keystore, final String certAlias, final String password) throws Exception {
		final Key key = (PrivateKey) keystore.getKey(certAlias, password.toCharArray());

		final Certificate cert = keystore.getCertificate(certAlias);
		final PublicKey publicKey = cert.getPublicKey();

		return new KeyPair(publicKey, (PrivateKey) key);
	}

	public static String encrypt(final byte[] data, X509Certificate encryptionCertificate) throws Exception {
		byte[] encryptedData = null;
		if (null != data && null != encryptionCertificate) {
			CMSEnvelopedDataGenerator cmsEnvelopedDataGenerator = new CMSEnvelopedDataGenerator();
			JceKeyTransRecipientInfoGenerator jceKey = new JceKeyTransRecipientInfoGenerator(encryptionCertificate);
			cmsEnvelopedDataGenerator.addRecipientInfoGenerator(jceKey);
			CMSTypedData msg = new CMSProcessableByteArray(data);
			OutputEncryptor encryptor = new JceCMSContentEncryptorBuilder(CMSAlgorithm.AES256_GCM).setProvider("BC").build();
			CMSEnvelopedData cmsEnvelopedData = cmsEnvelopedDataGenerator.generate(msg, encryptor);
			encryptedData = cmsEnvelopedData.getEncoded();
		}
		return new String(Base64.getEncoder().encodeToString(encryptedData));
	}

	public static String decrypt(final byte[] encryptedData, final PrivateKey decryptionKey) throws CMSException {
		byte[] decryptedData = null;
		if (null != encryptedData && null != decryptionKey) {
			CMSEnvelopedData envelopedData = new CMSEnvelopedData(Base64.getDecoder().decode(encryptedData));
			Collection<RecipientInformation> recip = envelopedData.getRecipientInfos().getRecipients();
			KeyTransRecipientInformation recipientInfo = (KeyTransRecipientInformation) recip.iterator().next();
			JceKeyTransRecipient recipient = new JceKeyTransEnvelopedRecipient(decryptionKey);
			decryptedData = recipientInfo.getContent(recipient);
		}
		return new String(decryptedData);
	}

}
