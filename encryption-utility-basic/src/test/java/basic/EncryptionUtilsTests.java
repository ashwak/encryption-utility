package basic;

import static org.junit.Assert.assertEquals;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.Test;

import br.com.lucasdall.encryption.EncryptionUtils;

public class EncryptionUtilsTests {

	@Test
	public void testEncryptDecryptStrings() throws Exception {
		System.out.println("#########################################");
		System.out.println("# BASIC - testEncryptDecryptStrings");
		System.out.println("#########################################");
		// Random private key
		final byte[] key = SecureRandom.getInstanceStrong().generateSeed(16);

		String plainMsg = "Hello World 2019 .....::::::||||||||||||| end";
		StopWatch sw = new StopWatch();
		sw.start();
		String enc = EncryptionUtils.encrypt(plainMsg.getBytes(StandardCharsets.UTF_8), key);
		sw.split();
		System.out.println(String.format("Encryption time : [%s]", sw));
		String dec = EncryptionUtils.decrypt(enc.getBytes(StandardCharsets.UTF_8), key);
		sw.split();
		System.out.println(String.format("Decryption time : [%s]", sw));

		System.out.println(String.format("Plain msg     : [%s]", plainMsg));
		System.out.println(String.format("Encrypted msg : [%s]", enc));
		System.out.println(String.format("Derypted msg  : [%s]", dec));
		
		assertEquals(plainMsg, dec);
	}

	@Test
	public void testEncryptDecryptFiles() throws Exception {
		System.out.println("#########################################");
		System.out.println("# BASIC - testEncryptDecryptFiles");
		System.out.println("#########################################");

		// Random private key
		final byte[] key = SecureRandom.getInstanceStrong().generateSeed(16);
		
		String plainFilePath = "/demo.xml";
		InputStream is = EncryptionUtilsTests.class.getResourceAsStream(plainFilePath);
		String plainFileContent = IOUtils.toString(EncryptionUtilsTests.class.getResourceAsStream(plainFilePath), StandardCharsets.UTF_8); 
		
		BufferedInputStream bis = new BufferedInputStream(is);
		StopWatch sw = new StopWatch();
		sw.start();
		String enc = EncryptionUtils.encrypt(bis, key);
		sw.split();
		System.out.println(String.format("Encryption time : [%s]", sw));
		String dec = EncryptionUtils.decrypt(enc.getBytes(StandardCharsets.UTF_8), key);
		sw.split();
		System.out.println(String.format("Decryption time : [%s]", sw));
		
		System.out.println(String.format("Plain file         : [%s]", plainFilePath));
		System.out.println(String.format("Plain file content : [%s]", plainFileContent));
		System.out.println(String.format("Encrypted msg      : [%s]", enc));
		System.out.println(String.format("Derypted msg       : [%s]", dec));
		
		assertEquals(plainFileContent, dec);
	}

}
