package bc;


import static org.junit.Assert.assertEquals;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.Test;

import br.com.lucasdall.encryption.EncryptionBCUtils;

public class EncryptionBCUtilsTests {

	private static KeyStore ks;
	private static KeyPair kp;
	private static X509Certificate cert;
	
	static {
		try {
			ks = EncryptionBCUtils.loadKeyStore(EncryptionBCUtils.class.getResourceAsStream("/demo.jks"), "teste", "jks");
			kp = EncryptionBCUtils.getKeyPair(ks, "teste", "");
			cert = (X509Certificate) ks.getCertificate("teste");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void testBCEncryptDecryptStrings() throws Exception {
		System.out.println("############################################");
		System.out.println("# BOUNCYCASTLE - testBCEncryptDecryptStrings");
		System.out.println("############################################");		
		String plainMsg = "Hello World BC 2019 .....::::::||||||||||||| end";
		StopWatch sw = new StopWatch();
		sw.start();
		String enc = EncryptionBCUtils.encrypt(plainMsg.getBytes(StandardCharsets.UTF_8), cert);
		sw.split();
		System.out.println(String.format("Encryption time : [%s]", sw));
		PrivateKey privateKey = kp.getPrivate();
		String dec = EncryptionBCUtils.decrypt(enc.getBytes(StandardCharsets.UTF_8), privateKey);
		sw.split();
		System.out.println(String.format("Decryption time : [%s]", sw));

		System.out.println(String.format("Plain msg     : [%s]", plainMsg));
		System.out.println(String.format("Encrypted msg : [%s]", enc));
		System.out.println(String.format("Derypted msg  : [%s]", dec));
		
		assertEquals(plainMsg, dec);
	}

	@Test
	public void testBCEncryptDecryptFiles() throws Exception {
		System.out.println("############################################");
		System.out.println("# BOUNCYCASTLE - testBCEncryptDecryptFiles");
		System.out.println("############################################");		

		String plainFilePath = "/demo.xml";
		InputStream is = EncryptionBCUtilsTests.class.getResourceAsStream(plainFilePath);
		String plainFileContent = IOUtils.toString(EncryptionBCUtilsTests.class.getResourceAsStream(plainFilePath), StandardCharsets.UTF_8); 
		
		PrivateKey privateKey = kp.getPrivate();

		BufferedInputStream bis = new BufferedInputStream(is);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		IOUtils.copy(bis, baos);
		is.close();
		baos.close();
		bis.close();
		StopWatch sw = new StopWatch();
		sw.start();
		String enc = EncryptionBCUtils.encrypt(baos.toByteArray(), cert);
		sw.split();
		System.out.println(String.format("Encryption time : [%s]", sw));
		String dec = EncryptionBCUtils.decrypt(enc.getBytes(StandardCharsets.UTF_8), privateKey);
		sw.split();
		System.out.println(String.format("Decryption time : [%s]", sw));
		
		System.out.println(String.format("Plain file         : [%s]", plainFilePath));
		System.out.println(String.format("Plain file content : [%s]", plainFileContent));
		System.out.println(String.format("Encrypted msg      : [%s]", enc));
		System.out.println(String.format("Derypted msg       : [%s]", dec));
		
		assertEquals(plainFileContent, dec);
	}

}
