package br.com.lucasdall.encryption;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;

public class EncryptionUtils {

	private static final String CIPHER = "AES/ECB/PKCS5Padding";
	
	private static final Charset ENCODING = StandardCharsets.UTF_8;

	/**
	 * Encrypts a String using a private key (16 bytes)
	 * 
	 * @param plainText
	 * @param key
	 * @return {@link String} coded in Base64
	 * @throws Exception
	 */
	public static String encrypt(byte[] plainText, byte[] key) throws Exception {
		Cipher cipher = Cipher.getInstance(CIPHER);
		SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);
		byte[] cipherText = cipher.doFinal(plainText);
		return Base64.encodeBase64String(cipherText);
	}

	/**
	 * Decrypts a base64 String
	 * 
	 * @param encryptedText - Base64 content
	 * @param key
	 * @return {@link String} with the plain content
	 * @throws Exception
	 */
	public static String decrypt(byte[] encryptedText, byte[] key) throws Exception {
		Cipher cipher = Cipher.getInstance(CIPHER);
		SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		byte[] cipherText = Base64.decodeBase64(encryptedText);
		return new String(cipher.doFinal(cipherText), ENCODING);
	}
	
	/**
	 * Encrypt - Shortcut for files 
	 * 
	 * @param in
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public static String encrypt(InputStream in, byte[] key) throws Exception {
		return encrypt(IOUtils.toByteArray(in), key);
	}
	
	/**
	 * 
	 * @param in
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public static String decrypt(InputStream in, byte[] key) throws Exception {
		return decrypt(IOUtils.toByteArray(in), key);
	}

}
